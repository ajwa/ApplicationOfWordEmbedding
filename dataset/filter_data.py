from pandas import read_csv
df = read_csv('reviews_csv_basic.csv')

df.columns=['reviews', 'value', 'cleanliness', 'room', 'location', 'service']
values = [ 1, 2, 3, 4, 5]
df_new = df[(df.reviews.str.len() < 1000) & df.value.isin(values) & df.cleanliness.isin(values) & df.room.isin(values) & df.location.isin(values) & df.service.isin(values)]

df_new.to_csv('All_Fresh_Reviews_filtered.csv')
