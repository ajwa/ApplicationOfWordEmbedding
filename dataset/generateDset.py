# importing csv module 
import csv 

# csv file name 
filename = "new-delhi.csv"
header = []
headerNew = ["Hotel Name", "Review Title", "Review", "Date"]
rows = []

# reading csv file 
with open(filename, 'r') as csvfile: 
	# creating a csv reader object 
	csvreader = csv.reader(csvfile) 
	header = csvreader.next()

	with open("reviews.csv", "w") as reviews:
		csvwriter = csv.writer(reviews)
		csvwriter.writerow(headerNew)
		count = 0
		for row in csvreader:
			hotelName = row[1]
			print hotelName + "\n"
			try:
				with open("new-delhi/" + row[0], 'r') as reviewFile:
					count += 1
					line = reviewFile.readline()
					while line:
						data = line.split("\t")
						newRow = []
						newRow.append(hotelName)
						newRow.append(data[1])
						newRow.append(data[2])
						newRow.append(data[0])
						csvwriter.writerow(newRow)
						line = reviewFile.readline()
			except IOError:
				print "***IO Error*** " + hotelName
		print "hotels >>>>>> " + str(count)
						

