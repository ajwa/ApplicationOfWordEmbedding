import json
import csv
import os

path = "/media/shivkumar/Windows/Users/shivk/Desktop/TripAdvisorJson/json/"
csv_file = open('/tmp/reviews_csv.csv', 'a')
count = 0
for filename in os.listdir(path):
    with open(path + filename, 'rU') as f:
        json_data = f.read()
        reviews_parsed = json.loads(json_data)
        reviews_data = reviews_parsed['Reviews']
        csvwriter =csv.writer(csv_file)
        for review in reviews_data:
            #print (review['Content']+","+review['Ratings']['Value']+","+review['Ratings']['Cleanliness']+","+review['Ratings']['Rooms']+","+review['Ratings']['Location']+","+review['Ratings']['Service'])
            #print ((review['Ratings'].keys()).contains)
            #if count == 0:
            #    header = review.keys()
            #    csvwriter.writerow(header)
            #    count += 1
            if all(item in review['Ratings'].keys() for item in ['Value', 'Cleanliness', 'Rooms', 'Location', 'Service']):
                tmp = [review['Content'], review['Ratings']['Value'], review['Ratings']['Cleanliness'], review['Ratings']['Rooms'], review['Ratings']['Location'], review['Ratings']['Service'] ]
                csvwriter.writerow(tmp)
'''
#open a file for reading
json_file = open('./TripAdvisorJson/json/72572.json', 'r')
json_data = json_file.read()
reviews_parsed = json.loads(json_data)
reviews_data = reviews_parsed['Reviews']

# open a file for writing
csv_file = open('/tmp/EmployData.csv', 'w')
# create the csv writer object
csvwriter = csv.writer(csv_file)
count = 0
for review in reviews_data:
    if count == 0:
        header = review.keys()
        csvwriter.writerow(header)
        count += 1
    csvwriter.writerow(review.values())
csv_file.close()
'''
