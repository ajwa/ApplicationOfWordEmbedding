import pandas as pd
import numpy as np
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from gensim.models import KeyedVectors

TRAIN_PERCENT = 0.7
MAX_NB_WORDS = 100000
MAX_SEQUENCE_LENGTH = 100
EMBEDDING_DIM = 100
#load glove model
EMBEDDING_FILE = 'glove.6B.100d.txt.word2vec'
model = KeyedVectors.load_word2vec_format(EMBEDDING_FILE, binary=False)

#load preprocessed dataset
dataset = pd.read_csv('preprocessed_dataset.csv')
dataset = dataset.sample(frac=1).reset_index(drop=True)

#split the dataset into train and test dataset
dataset_length = len(dataset)
train = dataset[0: dataset_length * TRAIN_PERCENT]['Review']
label_train = dataset[0: dataset_length * TRAIN_PERCENT].__getitem__(['value','cleanliness','room','location','service'])

test = dataset[dataset_length * TRAIN_PERCENT:]['Review']
label_test = dataset[dataset_length * TRAIN_PERCENT:].__getitem__(['value','cleanliness','room','location','service'])

#represent each word in corpus as interger
tokenizer = Tokenizer(num_words=MAX_NB_WORDS)
tokenizer.fit_on_texts(train + test)
train_sequences = tokenizer.texts_to_sequences(train)
test_sequences = tokenizer.texts_to_sequences(test)

word_index = tokenizer.word_index

#add padding to make length of each review same
train_padded = pad_sequences(train_sequences, maxlen=MAX_SEQUENCE_LENGTH)
label_train = np.array(label_train)

test_padded = pad_sequences(test_sequences, maxlen=MAX_SEQUENCE_LENGTH)
label_test = np.array(label_test)

nb_words = min(MAX_NB_WORDS, len(word_index)) + 1

#calculate word vector for each word in corpus and store it in list
embedding_matrix = np.zeros((nb_words, EMBEDDING_DIM))
for word, i in word_index.items():
    if word in word2vec.vocab:
        embedding_matrix[i] = word2vec.word_vec(word)

#Add embedding layer
embedding_layer = Embedding(nb_words,
        EMBEDDING_DIM,
        weights=[embedding_matrix],
        input_length=MAX_SEQUENCE_LENGTH,
        trainable=False)
#ADD LSTM layer
lstm_layer = LSTM(num_lstm, dropout=rate_drop_lstm, recurrent_dropout=rate_drop_lstm)
