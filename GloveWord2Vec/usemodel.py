import pandas as pd
from gensim.models import KeyedVectors
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy import spatial

#read dataset
train = pd.read_csv('preprocessed_dataset.csv')
train = train.sample(frac=1).reset_index(drop=True)
length = len(train['Review'])
train_percent = 0.6

#get Tfidf weights
vectorizer = TfidfVectorizer()
weights = vectorizer.fit_transform(train['Review'])
features = vectorizer.get_feature_names()

#load glove modal
filename = 'glove.6B.100d.txt.word2vec'
model = KeyedVectors.load_word2vec_format(filename, binary=False)
print "Model Loaded Successfully"

#calculate weighted average of word vectors
vectors = []
for i in range(length):
	numerator = 0
	denominator = 0
	for j, k in zip(*weights[i].nonzero()):
		if features[k] in model.vocab:
			numerator += ( model[features[k]] * weights[i][j, k] )
			denominator += weights[i][j, k]
	vectors.append(( numerator / denominator ))
print "Sentence Vectors Caluculated Successfully"

#test
total = 0
true = 0
tree = spatial.KDTree(vectors[:int(len(train['Review']) * train_percent)])
for i in range(int(len(train['Review']) * train_percent), length):
	p, q = tree.query(vectors[i], k=1)
	if train['value'][q] == train['value'][i]:
		true += 1
	if train['cleanliness'][q] == train['cleanliness'][i]:
		true += 1
	if train['room'][q] == train['room'][i]:
		true += 1
	if train['location'][q] == train['location'][i]:
		true += 1
	if train['service'][q] == train['service'][i]:
		true += 1
	total += 5

print total
print true
print "Accuracy :"
print  true * 100 / total
