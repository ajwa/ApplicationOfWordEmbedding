import pandas as pd
from gensim.models import KeyedVectors
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt

#read dataset
train = pd.read_csv('preprocessed_dataset.csv')

vectorizer = TfidfVectorizer()
weights = vectorizer.fit_transform(train['Review'])
features = vectorizer.get_feature_names()

#load glove modal
filename = 'glove.6B.100d.txt.word2vec'
model = KeyedVectors.load_word2vec_format(filename, binary=False)
print "Model Loaded Successfully"

tokens = []
labels = []
for i in range(1000, 2500, 10):
	if features[i] in model.vocab:
		tokens.append(model[features[i]])
		labels.append(features[i])

tsne_model = TSNE(perplexity=40, n_components=2, init='pca', n_iter=2500, random_state=23)
new_values = tsne_model.fit_transform(tokens)

x = []
y = []
for value in new_values:
	x.append(value[0])
	y.append(value[1])

plt.figure(figsize=(16, 16)) 
for i in range(len(x)):
	plt.scatter(x[i],y[i])
	plt.annotate(labels[i],
                     xy=(x[i], y[i]),
                     xytext=(5, 2),
                     textcoords='offset points',
                     ha='right',
                     va='bottom')
plt.show()
