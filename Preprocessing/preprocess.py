import pandas as pd
from nltk.corpus import stopwords
#from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

#read dataset
train = pd.read_csv('reviews.csv')

#convert all words to lowercase
train['Review'] = train['Review'].apply(lambda x: " ".join(x.lower() for x in str(x).split()))

#remove punctuation
train['Review'] = train['Review'].str.replace('[^\w\s]',' ')

#Remove Stopwords
stop = stopwords.words('english')
train['Review'] = train['Review'].apply(lambda x: " ".join(x for x in x.split() if x not in stop))

#remove most common words and rare words
'''freq = pd.Series(' '.join(train['Review']).split()).value_counts()[:50]
print freq

freq = list(freq.index)
train['Review'] = train['Review'].apply(lambda x: " ".join(x for x in x.split() if x not in freq))
'''

#stemming
'''st = PorterStemmer()
train['Review'] = train['Review'].apply(lambda x: " ".join([st.stem(word) for word in word_tokenize(x)]))
print train['Review'][0]'''


#lemmatization
lemmatizer = WordNetLemmatizer() 
train['Review'] = train['Review'].apply(lambda x: " ".join([lemmatizer.lemmatize(word) for word in word_tokenize(x)]))

#TODO : Drop rows with empty review
#train.dropna(subset=['Review'], inplace=True)

#save to csv
train.to_csv("preprocessed_dataset.csv")
